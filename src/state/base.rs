use super::arguments::Message;
use super::stackop::StackOperation;
use crate::frontend::ui::UI;

pub trait Base {
    fn on_enter(&mut self, _message: &Message, _ui: &mut UI) {}
    fn on_leave(&self, _ui: &mut UI) {}
    fn on_resume(&mut self, _message: &Message, _ui: &mut UI) {}
    fn on_suspend(&self) {}
    fn process(&mut self, _ui: &mut UI) -> StackOperation;
}
