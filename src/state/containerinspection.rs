use super::arguments::Message;
use super::base::Base;
use super::stackop::{continue_state, pop_state, StackOperation};
use crate::frontend::ui::UI;
use common::command::Command;

pub struct ContainerInspection {}

impl Base for ContainerInspection {
    fn on_enter(&mut self, _message: &Message, ui: &mut UI) {
        info!("entering container inspection");
        ui.create_container_window();
    }

    fn process(&mut self, ui: &mut UI) -> StackOperation {
        match ui.current_command() {
            Command::CONFIRM => {
                info!("confirmed");
                continue_state()
            }
            Command::QUIT => {
                ui.drop_container_window();
                pop_state()
            }
            _ => continue_state(),
        }
    }
}
