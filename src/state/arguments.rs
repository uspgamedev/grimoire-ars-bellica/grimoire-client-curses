use std::collections::HashMap;

use common::identity::ID;

pub struct Unused {}

pub struct EntitySelectionArg {
    pub selected_target_id: ID,
}

impl EntitySelectionArg {
    pub fn new(id: ID) -> Self {
        EntitySelectionArg {
            selected_target_id: id,
        }
    }
}

pub enum Argument {
    Unused(Unused),
    EntitySelectionArg(EntitySelectionArg),
    Bool(bool),
    ID(ID),
}

pub type Message = HashMap<String, Argument>;
