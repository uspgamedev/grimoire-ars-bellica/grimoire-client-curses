use std::collections::HashMap;

use super::arguments::{Argument, EntitySelectionArg, Message};
use super::base::Base;
use super::stackop::{continue_state, pop_state, StackOperation};
use crate::frontend::ui::UI;
use common::command::Command;
use common::intn::Int3;

pub struct EntitySelection {}

impl Base for EntitySelection {
    fn on_enter(&mut self, _message: &Message, _: &mut UI) {
        info!("Entering selection mode");
    }

    fn process(&mut self, ui: &mut UI) -> StackOperation {
        let cursor_scr = ui.cursor_position();

        ui.cursor_navigation();

        let session_state_ref = ui.maybe_session_state.as_ref().unwrap();
        let player_info = session_state_ref.player_info();
        let player_id = player_info.id;
        let player_position = player_info.maybe_position.as_ref().unwrap();

        let cursor_world = Int3::from(&cursor_scr) + Int3::new(0, 0, player_position.z);

        info!(
            "Cursor at {} {} {}",
            cursor_world.x, cursor_world.y, cursor_world.z
        );

        match ui.current_command() {
            Command::QUIT => {
                info!("Left quitting");
                pop_state()
            }
            Command::CONFIRM => {
                info!("Left selecting");
                match session_state_ref.get_entity_info_at(&cursor_world) {
                    Some(target_info) => {
                        let target_id = target_info.id;
                        info!(
                            "Selected {} {}",
                            target_id,
                            if target_id == player_id { "player" } else { "" }
                        );
                        let mut message = HashMap::new();
                        message.insert(
                            String::from("target_id"),
                            Argument::EntitySelectionArg(EntitySelectionArg::new(target_id)),
                        );
                        message.insert(
                            String::from("Target"),
                            Argument::EntitySelectionArg(EntitySelectionArg::new(target_id)),
                        );
                        pop_state().with_message(message)
                    }
                    None => {
                        info!("Selected {}", "floor");
                        pop_state()
                    }
                }
            }
            _ => continue_state(),
        }
    }
}
