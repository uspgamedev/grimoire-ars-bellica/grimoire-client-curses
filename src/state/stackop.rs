use super::arguments::Message;
use common::aliases::StringLiteral;

pub enum StackOperationType {
    CONTINUE,
    PUSH,
    POP,
    SWITCH,
}

pub struct StackOperation {
    pub op: StackOperationType,
    pub name: StringLiteral,
    pub message: Message,
}

impl StackOperation {
    pub fn new(op: StackOperationType) -> Self {
        StackOperation {
            op,
            name: "",
            message: Message::new(),
        }
    }

    pub fn with_name(mut self, name: StringLiteral) -> Self {
        self.name = name;
        self
    }

    pub fn with_message(mut self, message: Message) -> Self {
        self.message = message;
        self
    }
}

pub fn continue_state() -> StackOperation {
    StackOperation::new(StackOperationType::CONTINUE)
}

pub fn pop_state() -> StackOperation {
    StackOperation::new(StackOperationType::POP)
}

pub fn push_state(name: StringLiteral) -> StackOperation {
    StackOperation::new(StackOperationType::PUSH).with_name(name)
}

pub fn switch_state(name: StringLiteral) -> StackOperation {
    StackOperation::new(StackOperationType::SWITCH).with_name(name)
}
