use super::base::Base;
use super::stackop::{continue_state, pop_state, StackOperation};
use crate::frontend::ui::UI;
use common::command::Command;

pub struct LookMode {}

impl Base for LookMode {
    fn process(&mut self, ui: &mut UI) -> StackOperation {
        ui.cursor_navigation();
        match ui.current_command() {
            Command::QUIT => {
                info!("Exiting lookmode");
                pop_state()
            }
            _ => continue_state(),
        }
    }
}
