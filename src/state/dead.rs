use std::collections::HashMap;

use super::arguments::{Argument, Message};
use super::base::Base;
use super::stackop::{continue_state, pop_state, StackOperation};
use crate::frontend::ui::UI;
use common::command::Command;

pub struct Dead {}

impl Dead {
    pub fn on_leave(&self, ui: &mut UI) {
        ui.disable_input();
    }
}

impl Base for Dead {
    fn on_enter(&mut self, _message: &Message, ui: &mut UI) {
        ui.enable_input();
    }

    fn on_leave(&self, ui: &mut UI) {
        ui.disable_input();
    }

    fn process(&mut self, ui: &mut UI) -> StackOperation {
        match ui.current_command() {
            Command::QUIT => {
                info!("Exiting game");
                let mut message = HashMap::new();
                message.insert(String::from("quit"), Argument::Bool(true));
                pop_state().with_message(message)
            }
            _ => continue_state(),
        }
    }
}
