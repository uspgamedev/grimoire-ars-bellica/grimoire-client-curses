use super::arguments::Argument;
use super::arguments::Message;
use super::base::Base;
use super::stackop::{continue_state, pop_state, push_state, StackOperation};
use crate::frontend::ui::UI;
use common::session_response::*;
use std::io::{BufRead, BufReader};
use std::net::TcpStream;

pub struct Play {
    leave: bool,
    session_response_reader: BufReader<TcpStream>,
}

impl Play {
    pub fn new(session_stream: TcpStream) -> Self {
        Play {
            leave: false,
            session_response_reader: BufReader::new(session_stream),
        }
    }
}

impl Base for Play {
    fn on_resume(&mut self, message: &Message, _: &mut UI) {
        self.leave = match message.get("quit") {
            Some(b) => match b {
                Argument::Bool(bo) => *bo,
                _ => false,
            },
            None => false,
        };
    }

    fn on_enter(&mut self, _message: &Message, _: &mut UI) {
        self.leave = false;
    }

    fn process(&mut self, ui: &mut UI) -> StackOperation {
        if self.leave {
            return pop_state();
        }
        let mut buffer = String::new();
        info!("Waiting session response");
        self.session_response_reader.read_line(&mut buffer).unwrap();
        info!("sreaming in:\n{}", &buffer);
        match serde_json::from_str::<SessionResponse>(buffer.as_str()) {
            Ok(session_response) => match session_response {
                SessionResponse::StateChanged(session_state) => {
                    info!("Game state updated");
                    ui.maybe_session_state = Some(session_state);
                    continue_state()
                }
                SessionResponse::NoPlayer => push_state("DEAD"),
                SessionResponse::ActionPending(_) => push_state("USER_TURN"),
            },
            Err(_) => {
                warn!("Couldn't read session response");
                push_state("DEAD")
            }
        }
    }
}
