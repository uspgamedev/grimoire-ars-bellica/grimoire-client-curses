use std::boxed::Box;
use std::collections::HashMap;

use super::arguments::Message;
use super::base::Base;
use super::stackop::StackOperationType;
use crate::frontend::ui::UI;
use common::aliases::StringLiteral;

#[derive(Default)]
pub struct Stack {
    registered_states: HashMap<StringLiteral, Box<Base>>,
    state_stack: Vec<StringLiteral>,
}

impl Stack {
    pub fn register_state<T: Base + 'static>(&mut self, name: StringLiteral, state: T) {
        self.registered_states.insert(name, Box::new(state));
    }

    pub fn push(&mut self, name: StringLiteral) {
        let registered_state = self.registered_states.get(&name);
        if registered_state.is_some() {
            for state_name in &self.state_stack {
                if state_name == &name {
                    return;
                }
            }
            let current_state = self.current_state();
            if current_state.is_some() {
                (*current_state.unwrap()).on_suspend();
            }
            self.state_stack.push(name);
        }
    }

    pub fn push_with_message(&mut self, name: StringLiteral, message: &Message, ui: &mut UI) {
        self.push(name);
        if let Some(current_state) = self.current_state() {
            current_state.on_enter(message, ui);
        }
    }

    fn current_state(&mut self) -> Option<&mut Base> {
        match self.state_stack.last() {
            Some(last) => Some(self.registered_states.get_mut(last).unwrap().as_mut()),
            None => None,
        }
    }

    fn has_state(&self) -> bool {
        !self.state_stack.is_empty()
    }

    pub fn pop(&mut self, message: &Message, ui: &mut UI) {
        if self.state_stack.is_empty() {
            error!("Cannot pop from empty stack!");
            panic!();
        }
        self.current_state().unwrap().on_leave(ui);
        self.state_stack.pop();
        if !self.state_stack.is_empty() {
            self.current_state().unwrap().on_resume(message, ui);
        }
    }

    fn update(&mut self, ui: &mut UI) -> bool {
        match self.current_state() {
            None => false,
            Some(current_state) => {
                let mut changed = true;
                let operation = (*current_state).process(ui);
                match operation.op {
                    StackOperationType::CONTINUE => {
                        changed = false;
                    }
                    StackOperationType::PUSH => {
                        self.push_with_message(operation.name, &operation.message, ui);
                    }
                    StackOperationType::POP => {
                        self.pop(&operation.message, ui);
                    }
                    StackOperationType::SWITCH => {
                        self.pop(&Message::new(), ui);
                        self.push_with_message(operation.name, &operation.message, ui);
                    }
                }
                changed
            }
        }
    }

    pub fn main_loop(&mut self, ui: &mut UI) {
        while self.has_state() {
            while self.update(ui) {
                ui.flush();
            }
            if self.has_state() {
                ui.present();
            }
        }
        info!("UI ended");
    }
}
