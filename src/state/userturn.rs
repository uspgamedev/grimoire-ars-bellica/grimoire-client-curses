extern crate toml;

use super::arguments::{Argument, Message};
use super::base::Base;
use super::stackop::{continue_state, pop_state, push_state, StackOperation};
use crate::frontend::ui::UI;

use common::action_names::*;
use common::command::Command;
use common::datatable::DataTable;
use common::directions::Direction;
use common::intn::{Int2, Int3};
use common::session_request::SessionRequest;
use common::session_state::{find_entity_info, SessionState};

use std::collections::HashMap;
use std::io::Write;
use std::net::TcpStream;

pub struct UserTurn {
    leave: bool,
    turn_count: usize,
    server_stream: TcpStream,
}

impl UserTurn {
    pub fn new(server_stream: TcpStream) -> Self {
        UserTurn {
            leave: false,
            turn_count: 0,
            server_stream,
        }
    }
}

impl Base for UserTurn {
    fn on_resume(&mut self, message: &Message, ui: &mut UI) {
        info!("Resuming User Turn...");
        let entity_selection = match message.get("Target") {
            None => {
                return;
            }
            Some(msg) => match msg {
                Argument::EntitySelectionArg(arg) => arg,
                _ => {
                    error!("Wrong message sent to userturn::on_resume");
                    panic!();
                }
            },
        };
        let target_id = entity_selection.selected_target_id;
        info!("Received message: {}", target_id);
        let session_state_ref = ui.maybe_session_state.as_ref().unwrap();
        let player_id = session_state_ref.player_info().id;
        let parameters = datatable![
            ("actor_id", player_id),
            ("Self", player_id),
            ("target_id", target_id),
            ("Target", target_id)
        ];

        let target_info = find_entity_info(session_state_ref, target_id).unwrap();
        if !target_info.possible_actions.is_empty() {
            self.send_request(SessionRequest::TryAction(
                target_info.possible_actions[0].clone(),
                parameters,
            ));
            self.leave = true;
        }
    }

    fn on_enter(&mut self, _message: &Message, ui: &mut UI) {
        self.leave = false;
        ui.enable_input();
    }

    fn on_leave(&self, ui: &mut UI) {
        ui.disable_input();
    }

    fn process(&mut self, ui: &mut UI) -> StackOperation {
        if self.leave {
            return pop_state();
        }
        let maybe_movement = ui.entity_navigation();
        if let Some(session_state) = ui.maybe_session_state.as_ref() {
            self.turn_count += 1;
            info!("Starting turn #{}", self.turn_count);

            match maybe_movement {
                Some(movement_opt) => self.movement_actions(movement_opt, session_state),
                None => match ui.current_command() {
                    Command::INFO => push_state("LOOK_MODE"),
                    Command::QUIT => {
                        info!("Exiting game...");
                        self.send_request(SessionRequest::EndSession);
                        let mut message = HashMap::new();
                        message.insert(String::from("quit"), Argument::Bool(true));
                        pop_state().with_message(message)
                    }
                    Command::CONFIRM => self.interaction_actions(session_state),
                    Command::CANCEL => self.idle_action(session_state),
                    Command::INTERACT => self.pickup_actions(session_state),
                    Command::MENU => {
                        info!("pressed 'inv'");
                        push_state("CONTAINER_MODE")
                    }
                    _ => continue_state(),
                },
            }
        } else {
            continue_state()
        }
    }
}

impl UserTurn {
    fn movement_actions(&mut self, movement: Int3, session_state: &SessionState) -> StackOperation {
        let player_info = session_state.player_info();
        let next_position = player_info.maybe_position.as_ref().unwrap() + &movement;
        let other_entities = session_state.get_entity_infos_at(&next_position);
        let mut parameters = DataTable::default();
        parameters.insert(String::from("actor_id"), toml::Value::from(player_info.id));
        parameters.insert(String::from("Self"), toml::Value::from(player_info.id));
        for other_entity_info in other_entities {
            if !other_entity_info.passable {
                return match other_entity_info.possible_actions.first() {
                    Some(action) => {
                        parameters.insert(
                            String::from("target_id"),
                            toml::Value::from(other_entity_info.id),
                        );
                        parameters.insert(
                            String::from("Target"),
                            toml::Value::from(other_entity_info.id),
                        );
                        self.send_request(SessionRequest::TryAction(action.clone(), parameters));
                        pop_state()
                    }
                    None => continue_state(),
                };
            }
        }
        let dir = Direction::from(Int2::from(&movement));
        parameters.insert(
            String::from("direction"),
            toml::Value::from(dir.clone() as i64),
        );
        parameters.insert(String::from("Target"), toml::Value::from(player_info.id));
        parameters.insert(String::from("Dir"), toml::Value::from(dir as i64));
        self.send_request(SessionRequest::TryAction(MOVE.to_string(), parameters));
        info!("moving");
        pop_state()
    }

    fn interaction_actions(&mut self, session_state: &SessionState) -> StackOperation {
        if session_state.player_state.reach > 0 {
            return push_state("SELECT_MODE");
        }
        continue_state()
    }

    fn pickup_actions(&mut self, session_state: &SessionState) -> StackOperation {
        self.send_request(SessionRequest::TryAction(
            PICKUP.to_string(),
            datatable![("Self", session_state.player_info().id)],
        ));
        info!("picking up");
        pop_state()
    }

    fn idle_action(&mut self, session_state: &SessionState) -> StackOperation {
        self.send_request(SessionRequest::TryAction(
            IDLE.to_string(),
            datatable![("Self", session_state.player_info().id)],
        ));
        pop_state()
    }

    fn send_request(&mut self, session_request: SessionRequest) {
        let buffer = serde_json::to_string(&session_request).unwrap() + &("\n".to_string());
        info!("request streaming out:\n{}", &buffer);
        self.server_stream.write_all(buffer.as_bytes()).unwrap();
    }
}
