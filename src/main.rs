#[macro_use]
extern crate log;
extern crate chrono;
extern crate fern;
extern crate signal_hook;

use std::net::TcpStream;

use grimoire_client_curses::frontend::ui::{cleanup, UI};
use grimoire_client_curses::state::containerinspection::ContainerInspection;
use grimoire_client_curses::state::dead::Dead;
use grimoire_client_curses::state::entityselection::EntitySelection;
use grimoire_client_curses::state::lookmode::LookMode;
use grimoire_client_curses::state::play::Play;
use grimoire_client_curses::state::stack::Stack;
use grimoire_client_curses::state::userturn::UserTurn;

use std::io::Result;

fn main() -> Result<()> {
    setup_log()?;
    setup_hooks();
    run_client()?;
    cleanup();
    Ok(())
}

fn run_client() -> Result<()> {
    let session_stream = TcpStream::connect("127.0.0.1:1337")?;

    let mut stack = Stack::default();
    let mut ui = UI::new();

    stack.register_state("PLAY", Play::new(session_stream.try_clone().unwrap()));
    stack.register_state("USER_TURN", UserTurn::new(session_stream));
    stack.register_state("LOOK_MODE", LookMode {});
    stack.register_state("SELECT_MODE", EntitySelection {});
    stack.register_state("CONTAINER_MODE", ContainerInspection {});
    stack.register_state("DEAD", Dead {});

    info!("Setting initial game state...");

    stack.push("PLAY");

    stack.main_loop(&mut ui);

    Ok(())
}

fn setup_log() -> Result<()> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(fern::log_file("log.log")?)
        .apply()
        .unwrap();
    Ok(())
}

fn setup_hooks() {
    info!("Registering Interrupt signals...");
    unsafe {
        register_signal_hook(signal_hook::SIGINT);
        register_signal_hook(signal_hook::SIGQUIT);
    }
}

unsafe fn register_signal_hook(signal: i32) {
    let signal_result = signal_hook::register(signal, cleanup);
    if signal_result.is_err() {
        panic!("Error registering signal: {}", signal_result.err().unwrap());
    }
}
