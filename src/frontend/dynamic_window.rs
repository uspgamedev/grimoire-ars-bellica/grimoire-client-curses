use common::as_any::AsAny;
use common::intn::Int2;
use common::session_state::SessionState;
use std::any::Any;

pub trait DynamicWindow: Any + AsAny {
    fn adjust(&mut self, screen_size: Int2);
    fn show(&mut self, session_state: &SessionState);
}
