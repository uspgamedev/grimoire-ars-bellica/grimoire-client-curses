use super::dynamic_window::DynamicWindow;
use super::layout::{LOG_HEIGHT, STATUS_WIDTH};
use super::terminal::window::Window;
use common::intn::Int2;
use common::session_state::SessionState;

#[derive(Default)]
pub struct StatusWindow {
    win: Window,
}

impl DynamicWindow for StatusWindow {
    fn adjust(&mut self, screen_size: Int2) {
        self.win.reset(
            Int2::new(screen_size.x - STATUS_WIDTH, 0),
            Int2::new(STATUS_WIDTH, screen_size.y - LOG_HEIGHT),
        );
    }

    fn show(&mut self, session_state: &SessionState) {
        self.win.clear();
        self.win.box_();
        let player = &session_state.player_state;
        let player_hp = player.hp;
        let player_max_hp = player.max_hp;
        let text = format!("{}/{}", player_hp, player_max_hp);
        self.win.write(&Int2::new(1, 1), &text);
        self.win.refresh();
    }
}
