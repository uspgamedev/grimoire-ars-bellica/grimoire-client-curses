pub const LOG_HEIGHT: i64 = 8;
pub const LOG_LINES: i64 = LOG_HEIGHT - 2;
pub const STATUS_WIDTH: i64 = 42;
pub const MAP_WIDTH: i64 = 21;
pub const MAP_HEIGHT: i64 = 21;
