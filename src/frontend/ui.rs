use super::super::common::directions::DIRECTIONS;
use super::super::common::intn::{Int2, Int3, IntN};
use super::super::common::settings::Settings;
use super::container_window::ContainerWindow;
use super::log_window::LogWindow;
use super::map_window::MapWindow;
use super::status_window::StatusWindow;
use super::terminal::api as term_api;
use super::window_table::WindowTable;
use common::command::Command;
use common::session_state::*;

pub struct UI {
    windows: WindowTable,
    scrwidth: i32,
    scrheight: i32,
    current_cmd: Command,
    input_enabled: bool,
    settings: Settings,
    pub maybe_session_state: Option<SessionState>,
}

impl UI {
    pub fn new() -> Self {
        term_api::init();
        let mut windows = WindowTable::default();
        windows.add("map", MapWindow::default());
        windows.add("log", LogWindow::default());
        windows.add("status", StatusWindow::default());
        UI {
            windows,
            scrwidth: 0,
            scrheight: 0,
            current_cmd: { Command::INVALID },
            input_enabled: false,
            settings: Settings::default(),
            maybe_session_state: None,
        }
    }

    pub fn enable_input(&mut self) {
        self.input_enabled = true;
    }

    pub fn disable_input(&mut self) {
        self.input_enabled = false;
    }

    pub fn entity_navigation(&mut self) -> Option<Int3> {
        match self.windows.get_mut::<MapWindow>("map") {
            Some(mapwin) => {
                mapwin.lock_cursor();
                let movement = self.get_movement();
                if movement.size() > 0 {
                    Some(Int3::from(&movement))
                } else {
                    None
                }
            }
            None => None,
        }
    }

    pub fn current_command(&self) -> Command {
        self.current_cmd.clone()
    }

    pub fn cursor_navigation(&mut self) {
        let movement = self.get_movement();
        if let Some(mapwin) = self.windows.get_mut::<MapWindow>("map") {
            mapwin.unlock_cursor();
            if movement.size() > 0 {
                let new_pos = mapwin.get_cursor_position() + movement;
                mapwin.set_cursor_position(&new_pos);
            }
        }
    }

    pub fn cursor_position(&self) -> Int2 {
        self.windows
            .get::<MapWindow>("map")
            .unwrap()
            .get_cursor_position()
    }

    pub fn flush(&mut self) {
        self.current_cmd = Command::INVALID;
    }

    pub fn present(&mut self) {
        info!("PRESENTING");
        self.show_state();
        if self.input_enabled {
            self.current_cmd = self.poll_command();
        }
    }

    fn show_state(&mut self) {
        self.adjust_layout();
        if let Some(session_state) = self.maybe_session_state.as_ref() {
            debug!("{}", serde_json::to_string(session_state).unwrap());
            for window in self.windows.iter_mut() {
                window.show(session_state);
            }
        }
    }

    pub fn create_container_window(&mut self) {
        self.windows.add("inventory", ContainerWindow::default());
        self.force_adjust_layout();
    }

    pub fn drop_container_window(&mut self) {
        self.windows.remove("inventory");
    }

    pub fn get_movement(&self) -> Int2 {
        let idx = self.current_cmd.clone() as usize;
        if idx < DIRECTIONS.len() {
            DIRECTIONS[idx].clone()
        } else {
            Int2::default()
        }
    }

    pub fn force_adjust_layout(&mut self) {
        term_api::clear_and_refresh();
        let screen_size = Int2::new(i64::from(self.scrwidth), i64::from(self.scrheight));
        for window in self.windows.iter_mut() {
            window.adjust(screen_size.clone());
        }
    }

    pub fn adjust_layout(&mut self) {
        let (new_height, new_width) = term_api::get_screen_size();
        if new_height != self.scrheight || new_width != self.scrwidth {
            self.scrheight = new_height;
            self.scrwidth = new_width;
            self.force_adjust_layout();
        }
    }

    pub fn poll_command(&self) -> Command {
        let keybindings = self.settings.get_keybindings();
        let ch = term_api::get_input();
        match keybindings.get(ch) {
            Some(command) => command.clone(),
            None => Command::INVALID,
        }
    }
}

pub fn cleanup() {
    info!("Terminating curses");
    term_api::finish();
}
