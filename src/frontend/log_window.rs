use super::dynamic_window::DynamicWindow;
use super::layout::{LOG_HEIGHT, LOG_LINES};
use super::terminal::window::Window;
use common::intn::Int2;
use common::session_state::SessionState;

#[derive(Default)]
pub struct LogWindow {
    win: Window,
}

impl DynamicWindow for LogWindow {
    fn adjust(&mut self, screen_size: Int2) {
        self.win.reset(
            Int2::new(0, screen_size.y - LOG_HEIGHT),
            Int2::new(screen_size.x, LOG_HEIGHT),
        );
    }

    fn show(&mut self, session_state: &SessionState) {
        self.win.clear();
        self.win.box_();
        for i in 0..LOG_LINES {
            self.win.write(
                &Int2::new(1, LOG_HEIGHT - 2 - i),
                &session_state.message_log.message_entry(-1 - i),
            );
        }
        self.win.refresh();
    }
}
