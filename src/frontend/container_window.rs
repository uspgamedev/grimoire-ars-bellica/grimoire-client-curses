use super::dynamic_window::DynamicWindow;
use super::terminal::window::Window;
use common::intn::Int2;
use common::session_state::SessionState;

const HMARGIN: i64 = 5;
const VMARGIN: i64 = 2;

#[derive(Default)]
pub struct ContainerWindow {
    win: Window,
}

impl DynamicWindow for ContainerWindow {
    fn adjust(&mut self, screen_size: Int2) {
        self.win.reset(
            Int2::new(HMARGIN, VMARGIN),
            Int2::new(screen_size.x - 2 * HMARGIN, screen_size.y - 2 * VMARGIN),
        );
    }

    fn show(&mut self, session_state: &SessionState) {
        info!("showing container");
        self.win.clear();
        self.win.box_();
        let player = &session_state.player_state;
        let mut i = 1;
        for entity_info_idx in &player.inventory {
            let entity_info = &session_state.entities_info[*entity_info_idx];
            self.win.write(&Int2::new(1, i), &entity_info.name);
            info!("  entity {}", entity_info.name);
            i += 1;
        }
        self.win.refresh();
    }
}
