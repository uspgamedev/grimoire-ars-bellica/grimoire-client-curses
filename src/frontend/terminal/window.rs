use ncurses::*;

use super::colors::{to_native, ColorPairID};
use common::intn::Int2;

pub struct Window {
    win: Option<WINDOW>,
    pos: Int2,
    size: Int2,
}

impl Drop for Window {
    fn drop(&mut self) {
        info!("dropping window");
        if let Some(win) = self.win {
            self.clear();
            self.refresh();
            delwin(win);
        }
    }
}

impl Default for Window {
    fn default() -> Self {
        let pos = Int2::new(0, 0);
        let size = Int2::new(1, 1);
        let mut w = Window {
            win: None,
            pos: pos.clone(),
            size: size.clone(),
        };
        w.reset(pos, size);
        w
    }
}

impl Window {
    pub fn reset(&mut self, pos: Int2, size: Int2) {
        if self.win.is_some() {
            delwin(self.win.unwrap());
        }
        self.pos = pos;
        self.size = size;
        self.win = Some(newwin(
            self.size[1] as i32,
            self.size[0] as i32,
            self.pos[1] as i32,
            self.pos[0] as i32,
        ));
    }

    pub fn clear(&self) {
        werase(self.win.unwrap());
    }

    pub fn box_(&self) {
        box_(self.win.unwrap(), 0, 0);
    }

    pub fn write(&self, pos: &Int2, text: &str) {
        mvwaddstr(self.win.unwrap(), pos.y as i32, pos.x as i32, text);
    }

    pub fn refresh(&self) {
        wrefresh(self.win.unwrap());
    }

    pub fn highlight(&self, pos: &Int2) {
        mvwchgat(
            self.win.unwrap(),
            pos.y as i32,
            pos.x as i32,
            1,
            A_REVERSE(),
            to_native(ColorPairID::DEFAULT),
        );
    }
}
