extern crate ncurses;

pub enum ColorPairID {
    DEFAULT = 1,
}

use ncurses::constants::{COLOR_BLACK, COLOR_WHITE};

pub const fn to_native(id: ColorPairID) -> i16 {
    id as i16
}

pub fn color_pair_defs() -> [(i16, i16); 8] {
    [
        (COLOR_WHITE, COLOR_BLACK), // Unused
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
        (COLOR_WHITE, COLOR_BLACK),
    ]
}
