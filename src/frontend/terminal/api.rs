use ncurses::*;

use super::colors::color_pair_defs;

pub fn init() {
    setlocale(LcCategory::ctype, "");
    initscr();
    raw();

    keypad(stdscr(), true);
    noecho();
    cbreak();
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    start_color();
    for i in 1..color_pair_defs().len() {
        let (fg, bg) = color_pair_defs()[i];
        init_pair(i as i16, fg, bg);
    }

    info!("Initializing UI...");
    info!("Max color pairs: {}", COLOR_PAIRS());
    refresh();
}

pub fn get_screen_size() -> (i32, i32) {
    let mut new_height: i32 = 0;
    let mut new_width: i32 = 0;
    getmaxyx(stdscr(), &mut new_height, &mut new_width);
    (new_height, new_width)
}

pub fn get_input() -> char {
    getch() as u8 as char
}

pub fn clear_and_refresh() {
    clear();
    refresh();
}

pub fn finish() {
    clear_and_refresh();
    endwin();
}
