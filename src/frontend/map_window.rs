use super::dynamic_window::DynamicWindow;
use super::layout::{LOG_HEIGHT, MAP_HEIGHT, MAP_WIDTH, STATUS_WIDTH};
use super::terminal::window::Window;

use common::appearance::Appearance;
use common::glyph_palette::PALETTE;
use common::intn::{Int2, Int3, IntN};
use common::session_state::SessionState;
use common::tilemap::TileID;

const BEDROCK_LEVEL: usize = 0;

#[derive(Default)]
pub struct MapWindow {
    cursor_locked: bool,
    cursor_position: Int2,
    last_camera_pos: Int3,
    win: Window,
}

fn tile_glyph(tile_id: TileID) -> char {
    PALETTE.tile_to_glyph(&tile_id)
}

fn entity_glyph(appearance: Appearance) -> char {
    PALETTE.appearance_to_glyph(&appearance)
}

impl MapWindow {
    pub fn lock_cursor(&mut self) {
        self.cursor_locked = true;
    }

    pub fn unlock_cursor(&mut self) {
        self.cursor_locked = false;
    }

    pub fn get_cursor_position(&self) -> Int2 {
        self.cursor_position.clone()
    }

    pub fn set_cursor_position(&mut self, position: &Int2) {
        self.cursor_position = position.clone()
    }
}

impl DynamicWindow for MapWindow {
    fn adjust(&mut self, screen_size: Int2) {
        let base_x = (screen_size.x - STATUS_WIDTH) / 2 - (2 * MAP_WIDTH / 2 - 1);
        let base_y = (screen_size.y - LOG_HEIGHT) / 2 - MAP_HEIGHT / 2;
        self.win.reset(
            Int2::new(base_x, base_y),
            Int2::new(2 * MAP_WIDTH - 1, MAP_HEIGHT),
        );
    }

    fn show(&mut self, session_state: &SessionState) {
        let map = &session_state.visible_tilemap;
        let camera_pos = match session_state.entities_info[session_state.player_state.info_idx]
            .maybe_position
            .as_ref()
        {
            Some(position) => position.clone(),
            None => Int3::default(),
        };
        let center = Int2::new(MAP_WIDTH / 2, MAP_HEIGHT / 2);
        let scale = Int2::new(2, 1);
        self.win.clear();
        if self.cursor_locked {
            self.cursor_position = Int2::from(&camera_pos);
        }
        for y in (-MAP_HEIGHT / 2)..=(MAP_HEIGHT / 2) {
            for x in (-MAP_WIDTH / 2)..=(MAP_WIDTH / 2) {
                let mut pos = Int3::from(&(&self.cursor_position + Int2::new(x, y)));
                pos[2] = camera_pos.z;
                let offset = &center + Int2::new(x, y);
                let offset_times_scale = offset * &scale;
                if map.is_tile_inside(&pos) {
                    let tile_id = map.get_tile(&pos);
                    self.win.write(
                        &offset_times_scale,
                        &tile_glyph(tile_id.clone()).to_string(),
                    );
                    if tile_id == TileID::EMPTY
                        && (pos.get_k() == BEDROCK_LEVEL
                            || map.get_tile(&(pos - Int3::unit_k())) != TileID::EMPTY)
                    {
                        self.win.write(&offset_times_scale, &String::from("."));
                    }
                }
            }
        }

        for entity_info in &session_state.entities_info {
            match &entity_info.maybe_position {
                Some(position) => {
                    let entity_viewpos = &center + (Int2::from(position) - &self.cursor_position);
                    let appearance = entity_info.appearance.clone();
                    self.win.write(
                        &(entity_viewpos * &scale),
                        &entity_glyph(appearance).to_string(),
                    );
                }
                None => {}
            }
        }
        self.win.highlight(&(center * &scale));
        self.win.refresh();
        self.last_camera_pos = camera_pos.clone();
    }
}
