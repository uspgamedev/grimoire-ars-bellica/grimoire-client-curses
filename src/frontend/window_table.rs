use super::dynamic_window::DynamicWindow;
use std::boxed::Box;
use std::collections::HashMap;

#[derive(Default)]
pub struct WindowTable {
    windows: Vec<Box<dyn DynamicWindow>>,
    index_map: HashMap<&'static str, usize>,
}

impl WindowTable {
    pub fn add<T: DynamicWindow>(&mut self, name: &'static str, window: T) {
        self.windows.push(Box::new(window));
        self.index_map.insert(name, self.windows.len() - 1);
    }

    pub fn get<T: DynamicWindow>(&self, name: &'static str) -> Option<&T> {
        match self.index_map.get(name) {
            Some(index) => self.windows[*index].as_any().downcast_ref::<T>(),
            None => None,
        }
    }

    pub fn get_mut<T: DynamicWindow>(&mut self, name: &'static str) -> Option<&mut T> {
        match self.index_map.get(name) {
            Some(index) => self.windows[*index].as_any_mut().downcast_mut::<T>(),
            None => None,
        }
    }

    pub fn remove(&mut self, name: &'static str) {
        if let Some(index) = self.index_map.get(name) {
            self.windows.remove(*index);
            let index_cpy = *index;
            for (_, other_index) in self.index_map.iter_mut() {
                if *other_index > index_cpy {
                    *other_index -= 1;
                }
            }
        }
    }

    pub fn iter_mut(&mut self) -> std::slice::IterMut<Box<dyn DynamicWindow>> {
        self.windows.iter_mut()
    }
}
